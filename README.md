# whois

<!-- badges: start -->
<!-- badges: end -->

The goal of whois package is to provide an interface to build a local copy of 
RIR databases and create a tidy data frames with all information.

## Installation

By now, you need to clone [this](https://github.com/r-net-tools/RIR) github repository and build the R package with the following steps:

  1. Clone this repository `git clone https://github.com/r-net-tools/RIR.git`  
  2. Build whois package: `devtools::install()`  
  3. Load the new package: `library("whois")`  

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(whois)
afrinic <- afrinicToDF(verbose = TRUE)
```
[1] "[AFRINIC] Downloading source database..."
trying URL 'ftp://ftp.afrinic.net/pub/dbase/afrinic.db.gz'
downloaded 7.1 MB
[1] "[AFRINIC] Reading whois database TXT file: C:\\Temp\\RtmpARxK0V\\afrinic.db ..."
[1] "[AFRINIC] Searching for records..."
[1] "[AFRINIC] 261114 records found!"
[1] "[AFRINIC] Configuring multiple cores for that huge number of records!"
[1] "[AFRINIC] 5 cores selected for parsing records..."
[1] "[AFRINIC] Setting verbose mode off, I need concentration..."
[1] "[AFRINIC] I'm done! 5 cores free!"
[1] "[AFRINIC] 261114 parsed records!"
[1] "[AFRINIC] Building data frame..."
[1] "[AFRINIC] Data frame build: 261114x77"

## Code of Conduct
  
Please note that the whois project is released with a [Contributor Code of Conduct](https://contributor-covenant.org/version/2/1/CODE_OF_CONDUCT.html). 
By contributing to this project, you agree to abide by its terms.
