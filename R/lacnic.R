#' Download source database files for RIR-LACNIC
#'
#' @param verbose default as FALSE, set TRUE to show info messages while processing
#' @param rawpath character
#'
#' @return data.frame with info about downloaded files (url, name, local file, etc.)
downloadLACNIC <- function(verbose = FALSE, rawpath = "data-raw") {
  lacnic_url <- "ftp://ftp.lacnic.net/lacnic/dbase/lacnic.db.gz"

  if (!dir.exists(rawpath)) dir.create(rawpath)
  rawpath <- file.path(rawpath, "lacnic")
  if (!dir.exists(rawpath)) dir.create(rawpath)
  src.file <- file.path(rawpath, "lacnic.db")

  content <- RCurl::getBinaryURL(lacnic_url)
  if (verbose) print(paste("[LACNIC] Decompress and write encoded as UTF-8//TRANSLIT to ", src.file, sep = ""))
  str_content <- paste0(memDecompress(content, asChar = TRUE), "\n")
  str_content <- iconv(str_content, from = "ISO-8859-1", to = "UTF-8//TRANSLIT")

  write(str_content, file = src.file)

  return(src.file)
}

#' Download LACNIC DB, parse and build data frame
#'
#' @param rawpath character
#' @param verbose default as FALSE, set TRUE to show info messages while processing
#' @param boost numeric, default 100000
#'
#' @return data.frame
#' @export
lacnicToDF <- function(rawpath = "data-raw", verbose = FALSE, boost = 100000) {
  if (verbose) print("[LACNIC] Downloading source database...")
  lacnic.file <- downloadLACNIC(verbose = verbose, rawpath = rawpath)
  lacnic <- whoisToDF(lacnic.file, verbose, "LACNIC", boost = boost)
  lacnic <- lacnic[sort(names(lacnic))]

  return(lacnic)
}
