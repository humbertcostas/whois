#' Generic parser for whois records from RIR text data base files
#'
#' @param x character vector. It contains one whois record from reading text lines.
#'
#' @return data frame. One row with multiple variables representing a whois record.
parseWhoisrecord <- function(x) {
  rec <- tryCatch(
    {
      # Ref: https://miroslaw-zalewski.eu/blog/2015/reading-multiline-structured-file-format-in-r/
      x <- data.frame(name = as.character(x), stringsAsFactors = FALSE)
      x <- tidyr::separate(data = x, col = name,
                           into = c("name", "value"),
                           sep = ":\\s{1,}",
                           fill = "left", extra = "merge")

      # Ref: https://stackoverflow.com/questions/1782704/propagating-data-within-a-vector?noredirect=1&lq=1
      x$name <- zoo::na.locf(x$name)
      x$value <- trimws(x$value)

      # Ref: https://www.biostars.org/p/167028/
      x <- plyr::ddply(x, "name", plyr::summarize,
                       value = paste(value, collapse = " \n"))

      # Ref: https://stackoverflow.com/questions/6645524/what-is-the-best-way-to-transpose-a-data-frame-in-r-and-to-set-one-of-the-column
      out <- t(x[,2:ncol(x)])
      colnames(out) <- x[,1]
      out <- as.data.frame.matrix(out, stringsAsFactors = FALSE)
      out
    },
    error = function(cond) {
      message(paste("[ERROR] Bad record: class=", class(x), ", dimensions=", dim(x), sep = ""))
      message(paste("\n", as.character(unlist(x)), collapse = "\n"))
      message("Here's the original error message:")
      message(cond)
      # Choose a return value in case of error
      out <- data.frame(stringsAsFactors = F)
      out
    }
  )


  return(rec)
}

#' Given a local file of one RIR data base file as text, it parse and extract records to build a tidy data frame.
#'
#' @param whois.file path to RIR data base file as text.
#' @param verbose default as FALSE, set TRUE to show info messages while processing
#' @param rir only used if verbose is TRUE, it will be included in process messages
#'
#' @return data.frame
whoisToDF <- function(whois.file = "", verbose = FALSE, rir = ""){
  if (verbose) print(paste("[", rir, "] Reading whois database TXT file: ", whois.file, " ...", sep = ""))
  whois.raw <- readLines(whois.file, encoding = "UTF-8")

  if (verbose) print(paste("[", rir, "] Searching for records...", sep = ""))
  comments <- which(grepl("^#.*$", whois.raw))
  if (length(comments) > 0) whois.raw <- whois.raw[-comments]
  tags <- which(grepl("^%.*$", whois.raw))
  if (length(tags) > 0) whois.raw <- whois.raw[-tags]
  separator <- grepl("^$", whois.raw)
  record.limits <- cumsum(separator)
  nrec <- which(separator)
  record.limits[nrec] <- NA
  if (verbose) print(paste("[", rir,"] ", length(nrec)," records found!", sep = ""))
  rm(comments, tags, separator)

  boost <- length(nrec) > 50000
  if (boost) {
    if (verbose) print(paste("[", rir, "] Configuring multiple cores for that huge number of records!", sep = ""))
    no_cores <- parallel::detectCores() - 1
    if (verbose) print(paste("[", rir, "] ", no_cores, " cores selected for parsing records...", sep = ""))
    if (verbose) print(paste("[", rir, "] Setting verbose mode off, I need concentration...", sep = ""))
    whois.raw <- split(whois.raw, record.limits)
    cl <- parallel::makeCluster(no_cores)
    parallel::clusterExport(cl, "verbose", envir = environment())
    parallel::clusterExport(cl, "whois.raw", envir = environment())
    parallel::clusterExport(cl, "parseWhoisrecord", envir = environment())
    parsed <- parallel::parSapply(cl, whois.raw,
                                  function(x) {
                                    r <- parseWhoisrecord(x)
                                    r
                                  })
    parallel::stopCluster(cl)
    if (verbose) print(paste("[", rir, "] I'm done! ", no_cores, " cores free!", sep = ""))
    rm(cl, no_cores)
  } else {
    if (verbose) print(paste("[", rir, "] Parsing records...", sep = ""))
    prec <<- 1
    krec <<- 0
    parsed <- sapply(split(whois.raw, record.limits),
                     function(x) {
                       r <- parseWhoisrecord(x)
                       if (verbose) {
                         if ((floor(prec / 1000) - krec) > 0) {
                           print(paste("[", rir,"] ", floor(prec / 1000),"k records ok!", sep = ""))
                           krec <<- krec + 1
                         }
                       }
                       prec <<- prec + 1
                       r
                     })
  }
  rm(whois.raw, record.limits)
  if (verbose) print(paste("[", rir, "] ", length(nrec), " parsed records!", sep = ""))
  if (class(parsed) == "list") {
    if (verbose) print(paste("[", rir, "] Building data frame...", sep = ""))
    parsed <- plyr::rbind.fill(parsed)
    if (verbose) print(paste("[", rir, "] Data frame build: ", nrow(parsed), "x", length(names(parsed)), sep = ""))
  } else {
    if (verbose) print(paste("[", rir, "] Some records are wrong...", sep = ""))
    if (verbose) print(paste("[", rir, "][ERROR] Omited file ", whois.file, sep = ""))
    parsed <- data.frame(stringsAsFactors = FALSE)
  }

  return(parsed)
}

