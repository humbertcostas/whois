% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/parsewhois.R
\name{parseWhoisrecord}
\alias{parseWhoisrecord}
\title{Generic parser for whois records from RIR text data base files}
\usage{
parseWhoisrecord(x)
}
\arguments{
\item{x}{character vector. It contains one whois record from reading text lines.}
}
\value{
data frame. One row with multiple variables representing a whois record.
}
\description{
Generic parser for whois records from RIR text data base files
}
