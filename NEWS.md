# whois 0.7.0
* Finishing tests

# whois 0.5.0

* Working version. Tidy output with all object types and all attributes.

# whois 0.4.2

* Working version. Tidy output only include records with all mandatory attributes with data.

# whois 0.4.0

* Added a `NEWS.md` file to track changes to the package.
* First working version for all RIR data bases.
* One public function for each RIR: afrinicToDF, apnicToDF, and so on.
* Some RIR database files are huge and the process switch to parallelize parsing
  if it detect a file with more than 50k records. It will try to allocate all
  cores leaving only 1 for Operating System.
* All functions tested in i5 with 6 cores, 16Gb RAM and M2SSD HD spending near 4 hours.
